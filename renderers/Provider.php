<?php

namespace renderers;

class Provider {

    /**
     * @param string $page Page to load
     * @return BaseRenderer Renderer corresponding to $page
     */
    static function get_renderer($page) {
        $class = $page.'Renderer';
        if (!file_exists(__DIR__.'/'.str_replace('\\', '/',$class).'.php')) {
            $class = 'ErrorRenderer';
        }
        $renderer = 'renderers\\'.$class;
        return new $renderer();
    }
}