<?php

use app\BO\User;
use app\DAO\UserDAO;
use League\Container\Container;
use Vespula\Auth\Adapter\Ldap;
use Vespula\Auth\Auth;
use app\Session;
use PHPMailer\PHPMailer\PHPMailer;


$args = [
    'username' => FILTER_SANITIZE_STRING,
    'password' => FILTER_SANITIZE_STRING,
    'token' => FILTER_SANITIZE_STRING,
    'reset' => FILTER_VALIDATE_EMAIL,
    'from' => FILTER_SANITIZE_STRING,
];

$POST = filter_input_array(INPUT_POST, $args, false);

$ERROR = [
    'message' => true
];

$from = 'home';

// trying to login
if (isset($POST['username'])  && isset($POST['password'])) {
    $from = $POST['from'];
    if (!$SESSION->verifyToken('login_admin', $POST['token'])) {
        $ERROR = [
            'message' => "Invalid Token"
        ];
    }

    $field = str_contains($POST['username'], '@') ? 'email' : 'username';

    $credentials = [
        'username' => $POST['username'],
        'password' => $POST['password'],
    ];
    try {
        $user = $user_dao->find([$field => $credentials['username']]);
    } catch (\PDOException $e) {
        $ERROR = [
            'message' => 'Veuillez transmettre l\'erreur suivante à un administrateur : '.$e->getMessage()
        ];
        goto rendering;
    }


    try {
        $auth->login($credentials);
    } catch (\Vespula\Auth\Exception $e) {
        $ERROR = [
            'message' => $e->getMessage(),
        ];
        goto rendering;
    }
}

if($auth->isValid()){
    $from = $POST['from'] ?? 'home';
    if (strpos($from,'login')) {
        $form = 'home';
    }
    header('Location: index.php?page='.$from);
    exit();   
}else{
    if (isset($POST['username'])) {
        $ERROR['message'] = 'Veuillez vérifier vos valeurs de connexion';
    }
    $token = $SESSION->generateToken('login_admin');
}

rendering :
$renderer->header('Connexion')
            ->open_body([
                [
                    'tag' => 'div',
                ],
            ], false)
            ->error($ERROR['message'])
            ->login_form($token, $from)
            ->close_body()
            ->footer()
            ->render();