$('.btn-delete').click(function(){
    if (!confirm('Etes vous sûr(e) de vouloir archiver cet élément ?')) {
        stop()
    }
})



// General function to call ajax form
function call(page, options) {
    var formDatas = '';
    Object.keys(options.formDatas).forEach(function(key) {
        formDatas = formDatas + '&'+key+'='+options.formDatas[key]
    })
    $.ajax({
        url : 'index.php?page='+page,
        type : 'POST',
        data : formDatas.substring(1),
        dataType : options.dataType,
        success: function(data) {
            options.success(data)
        },
        error: function (data) {
            options.error(data)
        }
    })
}


function delete_instance(id, type) {
    options = {
        dataType: 'json',
        formDatas: {
            'delete': id,
        },
        success(data) {
            show_toast(data.type, data.message)
            reload_list(type)
        },
        error(data) {
            data = JSON.parse(data.responseText)
            show_toast(data.type, data.message)
            reload_list(type)
        }
    }
    call(type, options)
}

function edit_category(id) {
    options = {
        dataType: 'json',
        formDatas: {
            'edit': id,
            'edit_value': $('#cat_'+id+'-input').val()
        },
        success(data) {
            show_toast(data.type, data.message)
            reload_list('category')
        },
        error(data) {
            data = JSON.parse(data.responseText)
            show_toast(data.type, data.message)
            reload_list('category')
        }
    }
    call('category', options)
}

function add_category() {
    options = {
        dataType: 'json',
        formDatas: {
            'new': $('#new_category').val()
        },
        success(data) {
            show_toast(data.type, data.message)
            reload_list('category')
        },
        error(data) {
            data = JSON.parse(data.responseText)
            show_toast(data.type, data.message)
            reload_list('category')
        }
    }
    call('category', options)
}

function reload_list(list) {
    id = ''
    if (findGetParameter('id') !== null) {
        id = findGetParameter('id')
    }
    options = {
        dataType: 'html',
        formDatas: {
            'all': id,
        },
        success(data) {
            $('#'+list+'-container').html(data)
        },
        error(data) {
            $('#'+list+'-container').html(data.responseText)
        }
    }
    call(list, options)
}

function update_rights(u_id, i_id) {
    options = {
        dataType: 'json',
        formDatas: {
            'u_id': u_id,
            'i_id': i_id,
            'p_rights': event.target.value
        },
        success(data) {
            show_toast(data.type, data.message)
        },
        error(data) {
            data = JSON.parse(data.responseText)
            show_toast(data.type, data.message)
        }
    }
    call('permissions', options)
}

function search_user() {
    search = $('#user_search').val()
    if (search.length > 2) {
        options = {
            dataType: 'json',
            formDatas: {
                'search': search,
                'i_id': findGetParameter('id')
            },
            success(data) {
                $('#search-container').html(data)
            },
            error(data) {
                $('#search-container').html(data.responseText)
            }
        }
        call('permissions', options)
    }
}

function add_permission(u_id, i_id) {
    if (i_id === '') {
        save_indicator(u_id)
        return
    }
    options = {
        dataType: 'json',
        formDatas: {
            'u_id': u_id,
            'i_id': i_id,
            'p_rights': $('#p_rights').val()
        },
        success(data) {
            show_toast(data.type, data.message)
            reload_list('permissions')
            search_user()
        },
        error(data) {
            data = JSON.parse(data.responseText)
            show_toast(data.type, data.message)
            reload_list('permissions')
            search_user()
        }
    }
    call('permissions', options)
}

function save_indicator(u_id) {
    options = {
        dataType: 'json',
        formDatas: {
            'i_name': $('#i_name').val(),
            'i_category': $('#i_category').val(),
            'i_type': $('#i_type').val(),
            'i_frequency': $('#i_frequency').val(),
            'async': ''
        },
        success(data) {
            add_permission(u_id, data.id)
            location.href = '?page=edit&id='+data.id
        },
        error(data) {
            data = JSON.parse(data.responseText)
            add_permission(u_id, data.id)
            location.href = '?page=edit&id='+data.id
        }
    }
    call('edit', options)
}
$.fn.datepicker.dates["fr"] = {
    days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
    daysShort: ["Di", "Lu", "Ma", "Mer", "Je", "Ven", "Sam"],
    daysMin: ["Di", "Lu", "Ma", "Mer", "Je", "Ven", "Sam"],
    months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre"],
    monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Ju", "Juil", "Aout", "Sep", "Oct", "Nov", "Dec"],
    today: "Aujourd'hui",
    clear: "Clear",
    format: "dd/mm/yyyy",
    titleFormat: "MM",
    weekStart: 0,
};

$("#i_datepicker").datepicker( {
    changeYear: false,
    changeMonth: true,
    dateFormat: "dd/mm/YYYY",
    language: "fr",
    viewMode: "months",
    maxViewMode: "months",
    format: {
        toDisplay: function (date, format, language) {
            var d = new Date(date);

            return d.toLocaleDateString("fr-Fr", {
                day: "numeric",
                month: "numeric"
            });
        },
        toValue: function (date, format, language) {
            var d = new Date(date);
            return d.toLocaleDateString("fr-Fr", {
                day: "numeric",
                month: "2-digit",
                year: "numeric"
            });
        }
    }
}).focus(function () {
    $(".ui-datepicker-year").hide();
});

function init_modal(page, target_id) {
    options = {
        dataType: 'html',
        formDatas: {
            'modal_target': target_id
        },
        success(data) {
            $('#modal-content').html(data)
            $('#modal-name').html('Renseigner un indicateur')
        },
        error(data) {
            // data = JSON.parse(data.responseText)
            $('#modal-content').html(data.responseText)
            $('#modal-name').html('Renseigner un indicateur')
        }
    }
    call(page, options)
}

function init_preferences_modal(u_id) {
    options = {
        dataType: 'html',
        formDatas: {
            'prefs': ''
        },
        success(data) {
            $('#modal-content').html(data)
            $('#modal-name').html('Préférences')
        },
        error(data) {
            // data = JSON.parse(data.responseText)
            $('#modal-content').html(data.responseText)
            $('#modal-name').html('Préférences')
        }
    }
    call('indicators', options)
}

function init_warning_modal(i_id) {
    options = {
        dataType: 'html',
        formDatas: {
            'warning': i_id
        },
        success(data) {
            $('#modal-content').html(data)
            $('#modal-name').html('Envoyer un rappel')
        },
        error(data) {
            // data = JSON.parse(data.responseText)
            $('#modal-content').html(data.responseText)
            $('#modal-name').html('Envoyer un rappel')
        }
    }
    call('indicators', options)
}

function save_record() {
    options = {
        dataType: 'json',
        formDatas: {
            'r_indicator': $('#r_indicator').val(),
            'r_response': $('#r_response').val(),
            'r_effective_date' : $('#r_effective_date').val()
        },
        success(data) {
            show_toast(data.type, data.message)
            reload_list('indicators')

        },
        error(data) {
            data = JSON.parse(data.responseText)
            show_toast(data.type, data.message)
            reload_list('indicators')
        }
    }
    call('records', options)
}

function frequency_form(i_id) {
    options = {
        dataType: 'html',
        formDatas: {
            'form': event.target.value,
            'i_id': i_id
        },
        success(data) {
            $('#frequency-container').html(data)
        },
        error(data) {
            $('#frequency-container').html(data.responseText)
        }
    }
    call('edit', options)
}

function filter_category() {
    $('#cat_filter_form').submit()
}

function warn_late(i_id) {
    options = {
        dataType: 'json',
        formDatas: {
            'alert': i_id
        },
        success(data) {
            show_toast(data.type, data.message)
        },
        error(data) {
            data = JSON.parse(data.responseText)
            show_toast(data.type, data.message)
        }
    }
    call('indicators', options)
}

function new_category() {
    options = {
        dataType: 'html',
        formDatas: {
            'form': ''
        },
        success(data) {
            $('#modal-content').html(data)
            $('#new_category').focus()
        },
        error(data) {
            $('#modal-content').html(data.responseText)
            $('#new_category').focus()
            $('#category_modal').on('shown.bs.modal', function () {
                $('#category_modal').find('input').first().trigger('focus')
            })
        }
    }
    call('category', options)
}

function build_canva(datas, charttype, options) {
    var ctx = document.getElementById(datas[0]).getContext('2d');
    var chart = new Chart(ctx, {
        type: charttype,
        data: datas[1],
        options: options
    });
    return chart
}

function load_stats() {
    i_id = findGetParameter('id')
    $('#modal-content').html('<canvas id="chart" height="500px"></canvas>')
    options = {
        dataType: 'json',
        formDatas: {
            'stats': ''
        },
        success(data) {
            datas = []
            datas[0] =  'chart'
            datas[1] = {
                labels: data.labels,
                datasets: [{
                    label: data.datasets.label,
                    data: data.datasets.data,
                    fill: false,
                    borderColor: '#000000',
                }],
            }
            options =  {
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: false
                },
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                    }]
                },
            },
                console.log(datas)
            chart = build_canva(datas, 'line', options)
        },
        error(data) {
            data = JSON.parse(data.responseText)
            datas = []
            datas[0] =  'chart'
            datas[1] = {
                labels: data.labels,
                datasets: [{
                    label: data.datasets.label,
                    data: data.datasets.data,
                    fill: false,
                    borderColor: '#000000',
                }],
            }
            options =  {
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: false
                },
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                    }]
                },
            },
                console.log(datas)
            chart = build_canva(datas, 'line', options)
        }
    }
    call('history&id='+i_id, options)
}

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}