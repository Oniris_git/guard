<?php

namespace app\DAO;

class EnrolsDAO extends DAO
{
    protected $table = 'enrols';
    protected $prefix = 'e';

    public function enrol_users($enrols) {

        $request = 'INSERT INTO enrolments (mdl_user_id, guard_category_id, date) VALUES ';
        $binds = [];

        $date = date();

        $i = 0;
        foreach ($enrols as $guard_category => $users) {
            foreach ($users as $user) {
                if ($i != 0) {
                    $request .= ', ';
                }
                $request .= '(:userid'.$i.', guardid'.$i.', '.$date.')';
                $binds[':userid'.$i] = $user->getId();
                $binds[':guardid'.$i] = $guard_category;
                $i++;
            }
        }

        $this->getPDO()
                ->prepare($request.';')
                ->execute($binds);
    }

}