<?php
/**
 * Created by PhpStorm.
 * User: felicien.arnault
 * Date: 27/10/2021
 * Time: 10:01
 */

namespace app\DAO;

use app\BO\User;
use Vespula\Auth\Adapter\AdapterInterface;
use Vespula\Auth\Exception;


class UserDAO extends DAO implements AdapterInterface
{
    protected $table = 'moodle.mdl_user';
    protected $prefix = '';
    protected $moodle = true;

    public function find($params, $force_array = false, $joins = false, $order_by = '')
    {
        $joins = !$joins ? [] : $joins;

        $joins = array_merge($joins, [
            'indicators.admin' => [
                'join' => 'LEFT',
                'a_id' => 'id',
            ]
        ]);
        $datas = parent::find($params, true, $joins);
        $return = [];

        foreach ($datas as $row) {
            $user = new User($row);
            $user->setAdmin($row['a_id'] !== null);

            $return[] = $user;
        }


        return $this->force_array($return, $force_array);
    }

    public function search($input, $indicator_id) {
        $request = 'SELECT * FROM mdl_user
                      WHERE username LIKE :input 
                      AND mdl_user.deleted="0"
                      AND id NOT IN 
                      (SELECT p_user FROM indicators.permission WHERE p_indicator = :indicator_id);';

        $data = [];
        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':input' => "%$input%",
            ':indicator_id' => $indicator_id
        ]);
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row){
            $data[] = new User($row);
        }
        return $data;
    }

    /**
     * validate the username and password.
     *
     * @param array $credentials Array with keys 'username' and 'password'
     * @return boolean
     * @throws Exception
     */
    public function authenticate(array $credentials)
    {
        $field = str_contains($credentials['username'], '@') ? 'email' : 'username';

        $request = "SELECT * FROM mdl_user WHERE ".$field." = :username";

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':username' => $credentials['username']
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row) {
        }
        if (!empty($row)) {
            $check = password_verify($credentials['password'], $row['password']);
            if ($check == false) {
                throw new Exception("Mauvaise combinaison Identifiant/Mot de passe");
            }
            return $check;
        } else {
            throw new Exception("Mauvaise combinaison Identifiant/Mot de passe");
        }
    }

    /**
     * Find extra userdata. This will be stored in the session
     *
     * @param $username
     * @return User Userdata specific to the adapter
     */
    public function lookupUserData($username)
    {
        $field = str_contains($username, '@') ? 'email' : 'username';
        $request = "SELECT * FROM moodle.mdl_user LEFT JOIN indicators.admin ON id=a_id WHERE ".$field." = :username;";

        $stmt = $this->getPDO(true)->prepare($request);
        $stmt->execute([
            ':username' => $username
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $row) {
            $user = [
                'id' => $row['id'],
                'email' => $row['email'],
                'username' => $row['username'],
                'firstname' => $row['firstname'],
                'lastname' => $row['lastname'],
                'password' => $row['password'],
            ];
            $user = new User($user);
            $user->setAdmin($row['a_id'] !== null);

            return $user;
        }
    }

    public function getError()
    {
        // TODO: Implement getError() method.
    }

}