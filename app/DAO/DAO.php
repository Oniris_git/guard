<?php

namespace app\DAO;

use \PDO;

abstract class DAO extends \PDO
{
    protected $host;
    protected $user;
    protected $password;
    protected $db_name;
    protected $type;
    protected $pdo;
    protected $table;
    protected $prefix;
    protected $moodle = false;

    public function __construct(array $connector){
        $this->db_name= $connector['db_name'] ;
        $this->user= $connector['db_user'];
        $this->password= $connector['db_pass'];
        $this->host= $connector['db_host'];
        $this->type= $connector['db_type'];
    }

    /***
     * @param bool $moodle If need to use moodle database instead of indicators
     * @return PDO
     */
    protected function getPDO(){
        if(!isset($this->pdo)  || $this->moodle === true){
            $db_name = $this->moodle === true ? 'moodle' : $this->db_name;
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
            $this->pdo = new \PDO($this->type.':host=' . $this->host .
                ';dbname=' .$db_name,
                $this->user,
                $this->password,
                [
                    \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
                ]);
        }
        return $this->pdo;
    }

    public function persist($object) {
        if ($object->getId() !== null
            && $this->find([$this->prefix.'id' => $object->getId()]) !== false) {
            $update = true;
            $request = 'UPDATE '.$this->table.' SET ';
            foreach ($object->getAttributes() as $attr => $value) {
                $key = $attr;
                if (is_object($value)) {
                    $key = $this->prefix.$attr;
                }
                if ($key !== $this->prefix.'id') {
                    $request .= $key.' = :'.$key.', ';
                }
            }
            $request = substr($request, 0, -2);
            $request .= ' WHERE '.$this->prefix.'id = :'.$this->prefix.'id;';
        } else {
            $update = false;
            $request = 'INSERT INTO '.$this->table.' (';
            foreach ($object->getAttributes() as $attr => $value) {
                $key = $attr;
                if (is_object($value)) {
                    $key = $this->prefix.$attr;
                }
                $request .= $key.',';
            }
            $request = substr($request, 0, -1);
            $request .= ') VALUES (';
            foreach ($object->getAttributes() as $attr => $value) {
                $key = $attr;
                if (is_object($value)) {
                    $key = $this->prefix.$attr;
                }
                $request .= ':'.$key.',';
            }
            $request = substr($request, 0, -1);
            $request .= ');';
        }
        $binds = [];
        foreach ($object->getAttributes() as $attr => $value) {
            $bind = $value;
            $key = $attr;
            if (is_object($value)) {
                $bind = $value->getId();
                $key = $this->prefix.$attr;
            }
            $binds[':'.$key] = $bind;
        }
        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute($binds);

        if ($update === true) {
            return $object->getId();
        }
        if (isset($datas[$this->prefix.'id'])) {
            return $object->getId();
        }
        return $this->getPDO()->lastInsertId();
    }


    /**
     * General all function for entities not in BO
     * @return array
     */
    public function all($joins = false, $order_by = false) {
        if (in_array($this->table, [
            'category',
            'indicator',
            'record'
        ])) {
            $deleted = ' WHERE '.$this->prefix.'deleted = 0';
        } else {
            $deleted = '';
        }
        $request = 'SELECT * FROM '.$this->table;
        if ($joins !== false) {
            foreach ($joins as $table => $columns) {
                $join = $columns['join'];
                $request .= ' '.$join.' JOIN '.$table;
                unset($columns['join']);
                $i = 1;
                foreach ($columns as $inner => $outer) {
                    $word = $i === 1 ? 'ON' : 'AND';
                    $request .= ' '.$word.' '.$this->table.'.'.$outer.' = '.$table.'.'.$inner;
                    $i++;
                }
            }
        }
        $request .= $deleted;
        if ($order_by !== false) {
            $request .= ' ORDER BY '.$order_by;
        }
        return $this->getPDO()->query($request)->fetchAll();
    }

    /**
     * General find function for entities in BO
     * @param array $params associative array filter => value
     * @param boolean $force_array TRUE if result can be array. Used in specific find functions for entities in BO, ignored here
     * @param array $joins must be like :
     * [
            $table_to_join => [
                $col_in_current_table => $col_in_table_to_join
                $col_in_current_table => $col_in_table_to_join
            ]
       ]
     * @return array
     */
    public function find($params, $force_array = false, $joins = false, $order_by = ''){
        $request = 'SELECT * FROM '.$this->table;

        if ($joins !== false) {
            foreach ($joins as $table => $columns) {
                $join = $columns['join'];
                $request .= ' '.$join.' JOIN '.$table;
                unset($columns['join']);
                $i = 1;
                foreach ($columns as $inner => $outer) {
                    $word = $i === 1 ? 'ON' : 'AND';
                    $request .= ' '.$word.' '.$this->table.'.'.$outer.' = '.$table.'.'.$inner;
                    $i++;
                }
            }
        }
        $i = 0;
        $binds = [];
        foreach ($params as $filter => $value) {
            $word = $i === 0 ? ' WHERE ' : ' AND ';
            if ($value === null) {
                $request .= $word.$filter.' IS NULL';
            } else {
                $request .= $word.$filter.' = :value'.$i;
                $binds[':value'.$i] = $value;
            }
            $i++;
        }
        if ($order_by !== '') {
            $request .= ' ORDER BY '.$order_by;
        }
        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute($binds);
        $result = $stmt->fetchAll();
        $data = [];
        foreach ($result as $row) {
            $data[$row[$this->prefix.'id']] = $row;
        }

        return $this->force_array($data, $force_array);
    }

    /**
     * Delete
     * @param int $id Id to delete
     * @return true
     */
    public function delete($id) {
        $request = 'DELETE FROM '.$this->table.' WHERE '.$this->prefix.'id = :id';
        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':id' => $id
        ]);
        return true;
    }

    /**
     * Used for custom select requests
     * @param $request
     * @param false $force_array
     * @return array|false|mixed
     */
    public function select($request, $force_array = false) {
        $result = $this->getPDO()->query($request)->fetchAll();
        $data = [];
        foreach ($result as $row) {
            $data[$row[$this->prefix.'id']] = $row;
        }

        return $this->force_array($data, $force_array);
    }

    /**
     * Used for custom update requests
     * @param $request
     */
    public function update($request) {
        $this->getPDO()->exec($request);

        return true;
    }

    protected function force_array(&$data, $force_array) {
        switch (count($data)) {
            case 0 :
                if ($force_array === true) {
                    return [];
                }
                return false;
            case 1 :
                if ($force_array === true) {
                    return $data;
                }
                return reset($data);
            default : return $data;
        }
    }

}