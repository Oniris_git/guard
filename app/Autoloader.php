<?php
namespace app;

class Autoloader
{

    static function register()
    {
        spl_autoload_register(array(
            __CLASS__,
            'autoload'
        ));
    }

    static function autoload($class_name)
    {
        $app_path = realpath(__DIR__.DIRECTORY_SEPARATOR.'..').DIRECTORY_SEPARATOR;
        require $app_path.str_replace('\\', DIRECTORY_SEPARATOR, $class_name).'.php';
    }
}