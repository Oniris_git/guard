<?php
/**
 * Created by PhpStorm.
 * User: felicien.arnault
 * Date: 28/10/2021
 * Time: 15:44
 */

namespace app\BO;


class User
{
    use AppObject;

    private $id;
    private $email;
    private $firstname;
    private $lastname;
    private $password;
    private $admin;

    private int $enrols = 0;


    public function incrementEnrols() {
        $this->enrols++;

        return $this->getEnrols();
    }

    public function getEnrols() {
        return $this->enrols;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }


    /**
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->admin;
    }

    /**
     * @param mixed $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function toString() {
        return $this->getFirstname().'&nbsp;'.$this->getLastname();
    }



}