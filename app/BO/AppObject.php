<?php
/**
 * Created by PhpStorm.
 * User: felicien.arnault
 * Date: 03/11/2021
 * Time: 12:30
 */

namespace app\BO;


trait AppObject
{
    public function __construct($datas)
    {
        foreach ($datas as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }

    public function getAttributes() {
        return get_object_vars($this);
    }
}