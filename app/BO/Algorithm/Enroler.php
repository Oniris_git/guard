<?php

namespace app\BO\Algorithm;

use app\BO\User;

class Enroler
{
    /**
     * @var array
     * user_id => user
     */
    private array $users;

    /**
     * @var array
     * user_id => user
     */
    private array $reserve;

    /**
     * @var array
     * watch_id => [users]
     */
    private array $enrols;

    /**
     * @var int
     * Max enrols by any user
     */
    private int $entropy;

    private array $constraints;

    private Enroler $instance;

    private function __construct(){}

    private AlgorythmConfig $config;

    /**
     * @return Enroler
     */
    public function getInstance() {
        if (!isset($this->instance)) {
            $this->instance = new Enroler();
        }
        return $this->instance;
    }

    /**
     * @param $users
     * @return $this
     * Get all variables needed for the algorithm to work
     */
    public function init($users) {
       $this->users = $users;

       //Get all constraints
       foreach (scandir(__DIR__.'/Constraints') as $class) {
           if (!in_array($class, [
               'Constraint.php',
               'ConstraintFailedEvent.php'
           ])) {
               $class = str_replace('.php', '', $class);
               $constraint = new $class();
               $this->constraints[$constraint->getCode()] = $constraint;
           }
       }

       //(Re)set entropy
       $this->entropy = 0;

       //(Re)set reserve
       $this->reserve = [];
       return $this;
    }

    /**
     * @return $this
     * Launch auto enrol algorithm
     */
    public function shuffle(){
        do {
            if (!$this->hasUnenrolledUsers()) {
                $this->emptyReserve();
            }
            $index = rand(0, count($this->users));

            $user = $this->users[$index];

            try {
                $this->enrol($user);
            } catch (ConstraintFailedEvent $event) {
                $this->constraints[$event->getCode()]->processFailure($this);
            }
        } while('');

        return $this;
    }

    /**
     * @param User $user
     * @throws ConstraintFailedEvent
     */
    private function enrol(User $user) {
        foreach ($this->constraints as $constraint) {
            $constraint->attach($user)
                        ->validate($this);
        }

        $index = rand(0, count($this->enrols));

        $this->enrols[$index][$user->getId()] = $user;

        $this->reserve($user);

        return $this;
    }

    private function emptyReserve() {
        $this->users = $this->reserve;
        $this->reserve = [];

        return $this;
    }

    /**
     * @param $user
     * Move user in reserve
     */
    public function reserve($user) {
        $this->reserve[$user->getId()] = $user;
        unset($this->users[$user->getId()]);

        $this->entropy = max([
            $this->entropy,
            $user->incrementEnrols()
        ]);

        return $this;
    }

    public function hasUnenrolledUsers() {
        return count($this->users) > 0;
    }

    /**
     * @return int Entropy
     */
    public function getEntropy() {
        return $this->entropy;
    }

    /**
     * @return array
     */
    public function getEnrols(): array
    {
        return $this->enrols;
    }
}