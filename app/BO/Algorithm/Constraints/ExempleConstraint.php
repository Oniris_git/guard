<?php


use app\BO\Algorithm\Enroler;
use app\BO\User;

class ExempleConstraint extends Constraint
{

    protected int $code = 0;

    protected function validate(Enroler $enroler)
    {
        if ($enroler->hasUnenrolledUsers() && $this->user->getEnrols() > $enroler->getEntropy()) {
            throw new ExempleConstraintFailedEvent();
        }
    }

    protected function processFailure($enroler)
    {
        $enroler->reserve($this->user);
    }


}

class ExempleConstraintFailedEvent implements ConstraintFailedEvent {



    public function getCode()
    {
        return 0;
    }

    public function getMessage()
    {
        // TODO: Implement getMessage() method.
    }

    public function getFile()
    {
        // TODO: Implement getFile() method.
    }

    public function getLine()
    {
        // TODO: Implement getLine() method.
    }

    public function getTrace()
    {
        // TODO: Implement getTrace() method.
    }

    public function getTraceAsString()
    {
        // TODO: Implement getTraceAsString() method.
    }

    public function getPrevious()
    {
        // TODO: Implement getPrevious() method.
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
    }
}