<?php


use app\BO\Algorithm\Enroler;
use app\BO\User;

abstract class Constraint
{
    /**
     * @var int
     * Code of constraint
     */
    protected int $code;

    protected User $user;

    /**
     * @param Enroler
     * @return bool
     * @throws ConstraintFailedEvent
     */
    abstract protected function validate(Enroler $enroler);

    protected function attach(User $user) {
        $this->user = $user;

        return $this;
    }

    /**
     * Behaviour when failed
     */
    abstract protected function processFailure(Enroler $enroler);

    protected function getCode() {
        return $this->code;
    }

}