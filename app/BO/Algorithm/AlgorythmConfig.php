<?php

namespace app\BO\Algorithm;

use app\BO\AppObject;

class AlgorythmConfig
{

    use AppObject;

    private array $promotions;

    private array $exceptions;

    private array $guard_categories;

    private $start_date;

    private $end_date;


    /**
     * @return array
     */
    public function getPromotions(): array
    {
        return $this->promotions;
    }

    /**
     * @return array
     */
    public function getExceptions(): array
    {
        return $this->exceptions;
    }

    /**
     * @return array
     */
    public function getGuardCategories(): array
    {
        return $this->guard_categories;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

}